require 'test_helper'

class TopicsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get topics_index_path
    assert_response :success
  end

  test "should get show" do
    get topics_show_path(topics(:one))
    assert_response :success
  end

end
